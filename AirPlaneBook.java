import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AirPlaneBook {
    private static Scanner sc=new Scanner(System.in);
    private static  int[][] arr=new int[2][3];
    private static  int[][] arr1=new int[3][4];
    private static  int[][] arr2=new int[3][2];
    private static  int[][] arr3=new int[4][3];
    private static int count=1;
    private static List<int[][]> seatList=new ArrayList<>();
    private static int total_avail=0;
    private static boolean firstTimeCount=true;
    static {
        seatList.add(arr);
        seatList.add(arr1);
        seatList.add(arr2);
        seatList.add(arr3);
    }
    int section=4;

    public static void main(String[] args) {
        AirPlaneBook book=new AirPlaneBook();
        getChoice();
    }

    public static void  getChoice(){
        System.out.println("Welcome to AirPlane Booking System");
        System.out.println("1.Book Seats");
        System.out.println("2.Check Status of Seats");
        int choice=sc.nextInt();
        switch (choice){
            case 1:{
                showSeatAvailability();
                bookseat();
            } case 2:{
                showSeatAvailability();
                getChoice();
            } default:{
                System.out.println("Please Enter the Correct Choice");
                getChoice();
            }
        }
    }

    private static void bookseat() {
        System.out.println("1.Give your preference:");
        System.out.println("2.No Preference");
        int ch_pref=sc.nextInt();
        switch (ch_pref){
            case 1:{
                bookSeatPref();
            }
            case 2:{
                bookSeatWithOutPref();
            }
            default:{
                System.out.println("Please Choose the Correct Choice");
                getChoice();
            }
        }
    }

    private static void bookSeatWithOutPref() {
        System.out.println("Enter the number of seats you want to Book:");
        int seats=sc.nextInt();
        //to check all the seats are filled
        if((seats+(count-1))==total_avail){
            System.out.println("No Seats are Currently Available");
            return;
        }
        //if the no.of seats available is less
        if((seats+(count-1))>total_avail){
            System.out.println("Number of seats available is less than your choice ");
            System.out.println("Please Book Available seats");
        }
        else {
            while (seats > 0) {
                if (!bookAsileSeat()) {
                    if (!bookCenterSeat()) {
                        bookWindowsSeat();
                    }
                }
                seats--;
            }
            System.out.println("Seats booked Successfully");
        }
        getChoice();
    }

    private static void bookWindowsSeat() {
        for(int i=0;i<seatList.size();i++){
            if(i==0 || i==seatList.size()-1){
                int[][] sec=seatList.get(i);

                if(i==0) {
                    for (int col = 0; col < sec[0].length; col++) {
                        if (sec[0][col] == 0) {
                            sec[0][col] = count++;
                        }
                    }
                }
                else{
                    for (int col = 0; col < sec[0].length; col++) {
                        if (sec[i][col] == 0) {
                            sec[i][col] = count++;
                        }
                    }
                }
            }
        }
    }

    private static boolean bookCenterSeat() {
        boolean isBooked=false;
        for(int[][] seats:seatList) {
            for (int i = 0; i < seats.length ; i++) {
                //center seat is only available for col greater than 2
                for (int j = 1; j < seats[0].length-1; j++) {
                    if(seats[i][j]==0 && !isBooked){
                        seats[i][j]=count++;
                        isBooked=true;
                    }
                }
                if(isBooked){
                    break;
                }
            }
        }
        return isBooked;
    }

    private static boolean bookAsileSeat() {
        boolean isBooked=false;//tocheck whether seat is book or not
        for(int i=0;i<seatList.size();i++){
            //for the first section avoiding the first col since those will windows seat
            if(i==0){
                int[][] firstSec=seatList.get(0);
                for(int j=0;j<firstSec.length;j++){
                    if(firstSec[j][firstSec[0].length-1]==0){
                        firstSec[j][firstSec[0].length-1]=count++;
                        isBooked=true;
                        break;
                    }
                }
            }//for the last section avoiding the last col since those will windows seat
            else if(i==seatList.size()-1) {
                int[][]lastsec=seatList.get(i);
                for(int j=0;j<lastsec.length;j++){
                    if(lastsec[j][0]==0){
                        lastsec[j][0]=count++;
                        isBooked=true;
                        break;
                    }
                }
            }
            if(!isBooked && i!=0 && i!=seatList.size()-1){
                int[][] section=seatList.get(i);
                for(int a=0;a<section.length;a++){
                    for(int b=0;b<section[0].length;b++){
                        //aisle seats will either first col or last col
                        if ((b==0 || b==section[0].length-1)&& section[a][b]==0 && !isBooked ) {
                            section[a][b]=count++;
                            isBooked=true;
                            break;
                        }
                    }
                    if(isBooked){
                        break;
                    }
                }
            }
            if(isBooked){
                break;
            }
        }
        return isBooked;
    }

    private static void bookSeatPref() {
        System.out.println("Enter the number of seats you want to Book:");
        int seatCount=sc.nextInt();
        System.out.println("Enter the row and column you want to book");
        List<List<Integer>> bookseatList=new ArrayList<>();
        for(int i=0;i<seatCount;i++){
            List<Integer> list=new ArrayList<>();
            list.add(sc.nextInt());
            list.add(sc.nextInt());
            bookseatList.add(list);
        }
        for(List<Integer> list:bookseatList){
            int row=list.get(0)-1;
            int col=list.get(1)-1;
            boolean isBooked=false;
            for(int[][] seats:seatList) {
                int row_1=seats.length;
                int col_1=seats[0].length;
                if(row>=0&& col>=0 &&row<row_1 && col <col_1 && !isBooked ){
                    if(seats[row][col]==0) {
                        seats[row][col]=count;
                        count++;
                        isBooked=true;
                        break;
                    }
                }
            }
            if(!isBooked){
                System.out.println("The seat row-"+(row+1)+" and col-"+(col+1)+" is already booked");
            }
        }
        getChoice();
    }

    private static void showSeatAvailability() {
        int c=1;
        for(int[][] seats:seatList) {
            System.out.println("---Section-"+c+" ----");
            for (int i = 0; i < seats.length; i++) {
                for (int j = 0; j < seats[0].length; j++) {
                    System.out.print(" |" + seats[i][j] + "| ");
                    if(firstTimeCount)
                        total_avail++;
                }
                System.out.println();
            }
            c++;
            System.out.println("\t");
        }
        firstTimeCount=false;
        System.out.println("<<<<<<Total No.of Seats="+(total_avail-count)+">>>>>>>>");
    }
}
